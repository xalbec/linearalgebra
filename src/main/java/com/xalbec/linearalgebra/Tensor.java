package com.xalbec.linearalgebra;

public class Tensor {

    //Lets me know what kind of functionality the tensor should have
    int order;

    //Contains the information stored in the Tensor
    Object content;


    int rows;
    int columns;

    public Tensor(double[] vector){

        order = 1;
        content = vector;
        rows = vector.length;
        columns = 1;

    }

    public Tensor(double[][] matrix){

        order = 2;
        content = matrix;
        rows = matrix.length;
        columns = matrix[0].length;

    }

    //-------------------Static Methods------------------//
    //These methods will just return a new Tensor

    public static void mult(Tensor a, Tensor b){



    }

    //------------------Non-Static Methods-----------------//
    //These methods will make adjustments to the object calling them

}
