package com.xalbec.linearalgebra;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TensorTests {

    @BeforeAll
    public void setup(){

        Tensor vector1 = new Tensor(new double[]{1, 2, 3});
        Tensor vector2 = new Tensor(new double[]{4, 5, 6});
        Tensor vector3 = new Tensor(new double[]{1, 2, 3, 4});
        Tensor vector4 = new Tensor(new double[]{5, 6, 7, 8});

        Tensor matrix1 = new Tensor(new double[][]{
                                                    {1, 2, 3, 4},
                                                    {2, 3, 4, 5},
                                                    {3, 4, 5, 6},
                                                    {4, 5, 6, 7}});


    }


    @Test
    public void multiplyingProperMatricesReturnsProperValue(){



    }

}
